#!/usr/bin/env bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

#run redshift -l 5.6037168:-0.1869644 randr -v
run redshift
run mpd
run albert
