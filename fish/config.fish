#!/bin/fish

# add dirs to PATH
set PATH $PATH "$HOME/Packages"
set PATH $PATH "$HOME/Packages/flutter/bin"
set PATH $PATH "$HOME/Packages/genymotion"
set PATH $PATH "$HOME/Packages/android-studio/bin"
set PATH $PATH "$HOME/Packages/datagrip/bin"
set PATH $PATH "$HOME/Packages/phpstorm/bin"
set PATH $PATH "$HOME/Packages/ruby-mine/bin"
set PATH $PATH "$HOME/.pub-cache/bin"
set PATH $PATH "$HOME/Packages/flutter/.pub-cache/bin"
set PATH $PATH "$HOME/.gem/ruby/2.6.0/bin"
set PATH $PATH "$HOME/.gem/ruby/2.7.0/bin"
set PATH $PATH "$HOME/.local/bin"
set PATH $PATH "/opt/lampp/"
set PATH $PATH "$HOME/.config/composer/vendor/bin"
set PATH $PATH "/usr/lib/jvm/java-11-openjdk/bin"
set PATH $PATH "$HOME/.pub-cache/bin"
set DART_SDK "/opt/dart-sdk/bin"
set PATH $PATH "$HOME/.cargo/bin"
set IPYTHONDIR $IPYTHONDIR "$HOME/.ipython"
set ANDROID_NDK_HOME $ANDROID_NDK_HOME "$HOME/android/Sdk/ndk/21.1.6352462"
set ANDROID_SDK_ROOT ANDROID_SDK_ROOT "$HOME/android/Sdk"

# npm config
set NPM_PACKAGES "$HOME/.npm-packages"
set PATH $PATH $NPM_PACKAGES/bin
set MANPATH $NPM_PACKAGES/share/man $MANPATH  

# ========= START: Abbreviations =========
# git
abbr --add gcm git commit
abbr --add gmg git merge
abbr --add gco git checkout
abbr --add gcl git clone
abbr --add gp git push
abbr --add gpt git push --tags
abbr --add gpl git pull
abbr --add grb git rebase
abbr --add gf  git fetch
abbr --add gst git status
abbr --add gsth git stash
abbr --add gbl git branch -l
abbr --add gsr git subrepo
abbr --add grmt git remote
abbr --add gph git push heroku +HEAD:master
abbr --add gphf git push -f heroku HEAD:master


# pacman & yaourt
abbr --add pa sudo pacman
abbr --add pas sudo pacman -S
abbr --add par sudo pacman -R
abbr --add ya yaourt
abbr --add yas yaourt -S
abbr --add yar yaourt -R

# mpd & ncmpcpp others
abbr --add n ncmpcpp # mpd client
abbr --add nc ncmpcpp # mpd client
abbr --add mpcr mpc repeat on
abbr --add mpcs mpc single on
abbr --add m mpd

# taskwarrior abbreviations
abbr --add t task
abbr --add ta task add
abbr --add td task done
abbr --add t- task delete
abbr --add tdst task status:completed end.after:today all

# xampp abbreviations
abbr --add xstart sudo xampp start
abbr --add xstatus sudo xampp status
abbr --add xstop sudo xampp stop
abbr --add xrstart sudo xampp restart
abbr --add xrload sudo xampp reload

# tmux
abbr --add tma tmux attach -t
abbr --add tmn tmux new -s

# others
abbr --add fl flutter
abbr --add k kill
abbr --add pat php artisan
abbr --add e exa 
abbr --add pt poetry 
abbr --add ctl sudo systemctl
abbr --add less bat --style=plain
abbr --add bndl bundle exec 
abbr --add dic sdcv --color

# jupiter
abbr --add ipy jupyter qtconsole

# ========= END: Abbreviations =========

# jump package
# status --is-interactive; and (jump shell | psub)
#status --is-interactive; and source (jump shell fish | psub)
#status --is-interactive; and . (jump shell | psub)
#abbr --add j jump

# terminal colorizing
#starship init fish | source
source /etc/grc.fish
abbr --add ls ls --color=auto -lh
abbr --add grep grep --color=auto
abbr --add diff dff --color=auto

#source /home/ephrim/Packages/git-subrepo/.rc

# Afiewura mobile app production secrets
set LANDLORD_PARSE_SERVER_URL "https://server.afiewura.com/parse"

# Sdcv dictionary output format
set --global --export SDCV_PAGER less --quit-if-one-screen -RX
