# Defined in /home/ephrim/.config/fish/functions/clear-artisan-cache.fish @ line 1
function clear-artisan-cache
  php artisan config:clear
  php artisan cache:clear
  php artisan view:clear
  php artisan route:clear
  php artisan clear-compiled
  composer dump-autoload
end
