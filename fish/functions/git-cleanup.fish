function git-cleanup
	git branch --merged | egrep -v "(^\*|master|dev|testing|ephrim|prod|beta|alpha)\$" | xargs git branch -d
end
