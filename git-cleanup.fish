function git-cleanup
	git branch --merged | egrep -v "(^\*|master|dev|testing|ephrim)" | xargs git branch -d
end
